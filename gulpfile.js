const package = require("./package.json");

const gulp = require("gulp");
const sass = require("gulp-sass");

const autoprefixer = require("gulp-autoprefixer");
const cssnano = require("gulp-cssnano");
const cleanCSS = require("gulp-clean-css");
const sourcemaps = require("gulp-sourcemaps");

const rename = require("gulp-rename");
const plumber = require("gulp-plumber");
const gutil = require("gulp-util");

const concat = require("gulp-concat");
const babel = require("gulp-babel");
const uglify = require("gulp-uglify");

const imagemin = require("gulp-imagemin");

const browserSync = require("browser-sync").create();
const zip = require("gulp-zip");

const onError = function(err) {
    console.log("An error occurred:", gutil.colors.magenta(err.message));
    gutil.beep();
    this.emit("end");
};

gulp.task("images", (done) => {
    gulp
        .src("_src/images/**/*")
        .pipe(
            plumber({
                errorHandler: onError,
            })
        )
        .pipe(
            imagemin({
                optimizationLevel: 7,
                progressive: true,
            })
        )
        .pipe(gulp.dest("./assets/images"));
    done();
});

gulp.task("fonts", () => {
    return gulp.src(["./_src/fonts/**/*"]).pipe(gulp.dest("./assets/fonts/"));
});

gulp.task("sass", () => {
    return (
        gulp
        .src("./_src/sass/*.scss")
        .pipe(
            plumber({
                errorHandler: onError,
            })
        )
        .pipe(
            sass({
                outputStyle: "nested",
                precison: 3,
                errLogToConsole: true,
            })
        )
        .pipe(
            autoprefixer({
                cascade: false,
            })
        )
        .pipe(gulp.dest("./"))
        .pipe(sourcemaps.init())
        // .pipe(
        //   cssnano({
        //     preset: "last 1 versions",
        //   })
        // )
        .pipe(
            cleanCSS({ debug: true }, (details) => {
                console.log(`${details.name}: ${details.stats.originalSize}`);
                console.log(`${details.name}: ${details.stats.minifiedSize}`);
            })
        )
        .pipe(sourcemaps.write("."))
        .pipe(
            rename({
                suffix: ".min",
            })
        )
        .pipe(gulp.dest("./"))
    );
});

gulp.task("page-specific-js", () => {
    return gulp
        .src("./_src/js/page-specific/**/*")
        .pipe(
            babel({
                presets: ["@babel/env"],
            })
        )
        .pipe(
            uglify().on("error", function(e) {
                console.log(e);
            })
        )
        .pipe(gulp.dest("./assets/js/page-specific"));
});

gulp.task("vendor", () => {
    return gulp
        .src(["./_src/js/vendor/**/*"])
        .pipe(
            babel({
                presets: ["@babel/env"],
            })
        )
        .pipe(
            uglify().on("error", function(e) {
                console.log(e);
            })
        )
        .pipe(gulp.dest("./assets/js/vendor"));
});

gulp.task(
    "js",
    gulp.parallel("vendor", "page-specific-js", function() {
        return gulp
            .src([
                "_src/js/**/*",
                "!_src/js/vendor/**/*",
                "!_src/js/page-specific/**/*",
            ])
            .pipe(sourcemaps.init())
            .pipe(
                babel({
                    presets: ["@babel/env"],
                })
            )
            .pipe(concat("app.js"))
            .pipe(
                rename({
                    suffix: ".min",
                })
            )
            .pipe(
                uglify().on("error", function(e) {
                    console.log(e);
                })
            )
            .pipe(gulp.dest("./assets/js"));
    })
);

gulp.task("watch", function() {
    browserSync.init({
        files: ["./**/*.php"],
        proxy: "http://127.0.0.1/MINDK/YOVA/wordpress",
        //browser: "google chrome",
    });

    gulp.watch(
        ["./_src/sass/**/*.scss", "./_src/js/**/*.js", "./**/*.php"],
        gulp.series("sass", "js", reload)
    );
});

function reload(done) {
    browserSync.reload();
    done();
}

gulp.task("zip", function(done) {
    gulp
        .src(["!node_modules/**", "!node_modules", "!.vscode/**/*", "**/*"])
        .pipe(zip(package.name + ".zip"))
        .pipe(gulp.dest("./.."));

    done();
});

gulp.task("default", gulp.series("sass", "js", "fonts", "images"));
gulp.task("build", gulp.series("sass", "js", "images"));