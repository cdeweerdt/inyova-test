"use strict";

jQuery(document).ready(function($) {
  //   console.log("company-testimonial");
  $(".company-testimonial-user-nav a").on("click", function(e) {
    e.preventDefault();

    var _id = $(this).data("id");

    $(".company-testimonial.active").removeClass("active");
    $(".company-content-" + _id).addClass("active");

    $(".company-testimonial-user-nav a.active").removeClass("active");
    $(this).addClass("active");
  });
});
