"use strict";

jQuery(document).ready(function($) {
  $(".js-french-cta").on("click", function() {
    $(this).hide();
    $(this)
      .next(".french-subscribe-form-holder")
      .addClass("is-active");
  });
});
