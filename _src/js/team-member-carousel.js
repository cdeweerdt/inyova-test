"use strict";

jQuery(document).ready(function($) {
  $(window).scroll(function() {
    var scroll = $(window).scrollTop();
    if (scroll >= 120) {
      $(".mobile-team-member-nav").addClass("active");
    } else {
      $(".mobile-team-member-nav").removeClass("active");
    }
  });

  const _holder = $(".team-members-carousel-holder");
  const _target = $(".team-member-carousel");
  if (_target) {
    $(_target).owlCarousel({
      items: 1,
      margin: 20,
      nav: false,
      dots: false,
      responsiveClass: true,
      responsive: {
        680: {
          items: 2
        },
        1024: {
          items: 4
        },
        1921: {
          items: 3
        }
      }
    });

    _holder.find(".carousel-next-btn").click(function() {
      _target.trigger("next.owl.carousel");
    });
    // Go to the previous item
    _holder.find(".carousel-prev-btn").click(function() {
      _target.trigger("prev.owl.carousel");
    });
  }

  const board_holder = $(".team-board-members-carousel-holder");
  const board_target = $(".team-board-member-carousel");

  if (board_target) {
    $(board_target).owlCarousel({
      items: 2,
      margin: 20,
      nav: false,
      dots: false,
      responsive: {
        680: {
          items: 3
        },
        1024: {
          items: 4,
          // rtl: true
        }
      }
    });

    board_holder.find(".carousel-next-btn").click(function() {
      board_target.trigger("next.owl.carousel");
    });
    // Go to the previous item
    board_holder.find(".carousel-prev-btn").click(function() {
      board_target.trigger("prev.owl.carousel");
    });
  }
});
