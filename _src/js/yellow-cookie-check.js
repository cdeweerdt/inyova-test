"use strict";
// check yova yellow page and set cookie

jQuery(document).ready(function($) {
  var _domain = window.location.host;
  if (
    document.referrer.indexOf(_domain) == -1 &&
    $("body").hasClass("page-template-yova-yellow")
  ) {
    // it's first time and it's YY page
    // set yova_pti 6
    setCookie("yova_pti", "6", 7);
  }
});
