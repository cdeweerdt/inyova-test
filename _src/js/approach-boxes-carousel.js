"use strict";

jQuery(document).ready(function ($) {
  function makeApproachCarousel() {
    if ($(".approach-banner-boxes-holder").length > 0) {
      if ($(window).width() <= 768) {
        $(".approach-banner-scroll-content").each(function () {
          // console.log("destroy");
          $(this).trigger("destroy.owl.carousel");
        });
      } else {
        $(".approach-banner-scroll-content").each(function (item) {
          var nextBtn = $(this).parent().parent().find(".carousel-next-btn");
          var prevBtn = $(this).parent().parent().find(".carousel-prev-btn");

          var _count = $(this).find(".approach-banner-box").length;

          var $carousel = $(this).owlCarousel({
            nav: false,
            // dots: false,
            responsive: {
              0: {
                items: 1,
                margin: 20,
                stagePadding: _count > 1 ? 20 : 0,
              },
              768: {
                items: 2,
                margin: 30,
                stagePadding: _count > 2 ? 80 : 0,
              },
            },
          });

          nextBtn.click(function () {
            $carousel.trigger("next.owl.carousel");
          });
          // Go to the previous item
          prevBtn.click(function () {
            $carousel.trigger("prev.owl.carousel");
          });
        });
      }
    }
  }

  $(window).resize(function () {
    makeApproachCarousel();
  });
});
