"use strict";

jQuery(document).ready(function ($) {
  // Create Tooltip

  function rePositionTooltip() {
    if ($(window).width() <= 768) {
      // console.log("rePositionTooltip");
      $(".tooltip").each(function () {
        var tooltip = $(this).find(".tooltiptext");
        var tooltip_pos = $(this).position();

        var tooltip_width = $(this).width();
        var parent_width = $(this).parent().width();

        var new_left = ((parent_width - tooltip_width) / 2) * -1;

        if (tooltip_pos.left <= $(window).width() / 3) {
          new_left = -40;
        }
        // console.log("re-position tooltip", tooltip_pos.left, );
        // console.log("parent_width", parent_width, tooltip_pos.left, new_left);

        if ($(window).width() >= 680) {
          new_left = tooltip_pos.left < 100 ? 0 : new_left;
        }

        $(this).find(".tooltiptext").css("left", new_left);
      });
    } else {
      $(".tooltip").each(function () {
        $(this).find(".tooltiptext").css("left", "50%");
      });
    }
  }

  $(window).resize(function () {
    // console.log("resize");
    rePositionTooltip();
  });

  if ($(".remove-yy-cookie").length) {
    $(".remove-yy-cookie").on("click", function (e) {
      e.preventDefault();
      var _href = $(this).attr("href");
      document.cookie =
        "yova_pti=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
      window.location = _href;
    });
  }

  if ($(".show-password-holder").length) {
    var _showPass = false;
    $(".show-password-holder").on("click", function () {
      $(this).toggleClass("is-show");

      const $type = _showPass ? "password" : "text";
      // console.log($type);
      $("#password").prop("type", $type);
      _showPass = !_showPass;
    });
  }

  $(".scroll-btn").on("click", function () {
    var position =
      $(".scroll-target").offset().top - $(".site-header").outerHeight() - 50;
    $("html, body").animate(
      {
        scrollTop: position,
      },
      "slow"
    );
  });

  if ($(".js-history-back").length) {
    var site_url = $("meta[name=site-url]").attr("content");
    $(".js-history-back").on("click", function (e) {
      // e.preventDefault();

      var href = $(this).attr("href");
      if (href == "javascript:;") {
        if (document.referrer == "") {
          window.location.href = site_url;
        } else {
          window.history.back();
        }
      }
    });
  }

  if ($(".js-banner-box-btn").length) {
    $(".js-banner-box-btn").on("click", function (e) {
      // console.log(" banner box");
      e.preventDefault();
      $(this).parent().toggleClass("active");
    });
  }

  $(".hamburger-menu-btn").on("click", function (e) {
    e.preventDefault();
    $(this).toggleClass("is-open");
    $(".mobile-menu-holder").toggleClass("is-open");
    $(".mobile-menu-holder").slideToggle(300);
  });

  $(window).resize(function () {
    if ($(window).width() >= 1024) {
      $(".mobile-menu-holder").show();
    }

    if ($(".whatsapp-link").length) {
      var desktop_whatapp_link = "https://web.whatsapp.com/send?phone=";
      var mobile_whatapp_link = "https://wa.me/";
      var whatsapp_link = $(".whatsapp-link a").attr("href");
      // whatsapp link update
      var md = new MobileDetect(window.navigator.userAgent);
      // console.log(md);
      if (md.mobile()) {
        var mobile_link = whatsapp_link.replace(
          desktop_whatapp_link,
          mobile_whatapp_link
        );
        $(".whatsapp-link a").attr("href", mobile_link);
      } else {
        var desktop_link = whatsapp_link.replace(
          mobile_whatapp_link,
          desktop_whatapp_link
        );
        $(".whatsapp-link a").attr("href", desktop_link);
      }
    }
  });

  $(window).trigger("resize");
});
