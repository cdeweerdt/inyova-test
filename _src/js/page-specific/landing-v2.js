jQuery(document).ready(function ($) {
  var isGoogleAuth = false;

  var googleUser = {};
  var _auth2;

  var startGoogleAuth = function () {
    // console.log("startGoogleAuth");
    gapi.load('auth2', function () {
      _auth2 = gapi.auth2.init({
        client_id: yova_ajax_landing_params.google_client_id,
        cookiepolicy: 'single_host_origin',
        // Request scopes in addition to 'profile' and 'email'
        //scope: 'additional_scope'
      });
      enableGoogleButton();
    });
  };

  function enableGoogleButton() {
    _auth2.attachClickHandler(
      document.getElementById('google-sign-up'),
      {},
      function (googleUser) {
        googleSignIn(googleUser);
      },
      function (error) {
        console.log(JSON.stringify(error, undefined, 2));
        $('.landing-v2-hero-form').find('.preloader').removeClass('active');
      }
    );
  }

  function googleSignIn(googleUser) {
    var _profile = googleUser.getBasicProfile();
    var _uid = _profile.getId();
    var _name = _profile.getName();
    var _last_name = _profile.getFamilyName();
    var _email = _profile.getEmail();

    var _medium_source = $('.landing-v2-hero-form').data('medium-source');
    var _page_id = $('.landing-v2-hero-form').data('page-id');
    // clear form
    $('.landing-form').find('.error-holder').remove();
    $('.landing-form').find('.has-error').removeClass('has-error');

    var registerFields = {
      email: _email,
      first_name: _name,
      last_name: _last_name,
      source: 'Website Gmail',
    };

    var registerData = {
      action: 'yova_track_register',
      lead: registerFields,
      security: yova_ajax_params.security,
      page_id: _page_id,
    };

    trackingRegisterData(registerData);

    // Check if request is already exist
    // if (xhrLanding !== null && xhrLanding.readyState !== 4) return false;

    var lead = {
      email: _email,
      first_name: _name,
      last_name: _last_name,
      uid: _uid,
      password: '',
      provider: 'google',
    };

    var emailao = lead.email;
    if (lead.email) {
      // Set ajax data
      var data = {
        action: 'yova_create_leads',
        lead: lead,
        security: yova_ajax_params.security,
        page_id: _page_id,
        mediumSource: _medium_source,
      };

      // landing.js
      makeLandingRequest(data);
    }
  }

  $('#google-sign-up').on('click', function () {
    isGoogleAuth = true;

    $('.landing-v2-hero-form').find('.preloader').addClass('active');

    $('.landing-form').find('input').val('');

    $('.landing-form').find('.has-error').removeClass('has-error');

    $('.landing-form').find('.error-holder').remove();

    $(this).removeClass('has-error');
    $('.general-error-holder').val('');
  });

  startGoogleAuth();

  // clear email2 honeypot input on opening
  $(this).find('[name="email2"]').val('');

  var xhrLanding = null;

  function validateEmail(email) {
    var re =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  $('.landing-form').on('submit', function (e) {
    e.preventDefault();

    isGoogleAuth = false;
    $('.form-form-holder .general-error-holder').empty();

    $(this).find('.has-error').removeClass('has-error');

    $(this).find('.error-holder').remove();

    var _first_name = $(this).find('[name="first_name"]');
    var _last_name = $(this).find('[name="last_name"]');
    var _email = $(this).find('[name="email"]');
    var _email2 = $(this).find('[name="email2"]');
    var _password = $(this).find('[name="password"]');
    var _optin = $(this).find('[name="optin"]');

    var _page_id = $('.landing-v2-hero-form').data('page-id');

    var registerFields = {
      email: _email.val(),
      first_name: _first_name.val(),
      last_name: _last_name.val(),
      source: 'Website Email',
    };

    var registerData = {
      action: 'yova_track_register',
      lead: registerFields,
      security: yova_ajax_params.security,
      page_id: _page_id,
    };

    trackingRegisterData(registerData);

    var _medium_source = $('.landing-v2-hero-form').data('medium-source');

    if (_first_name.val().length < 1) {
      _first_name.addClass('has-error');
      $(_first_name).after('<span class="error-holder">' + $(_first_name).data('message') + '</span>');
      return false;
    }

    if (_last_name.val().length < 1) {
      _last_name.addClass('has-error');
      $(_last_name).after('<span class="error-holder">' + $(_last_name).data('message') + '</span>');
      return false;
    }

    if (!validateEmail(_email.val())) {
      _email.addClass('has-error');
      $(_email).after('<span class="error-holder">' + $(_email).data('message') + '</span>');
      return false;
    }

    if (_password.val().length < 6) {
      _password.addClass('has-error');
      $('.password-check-bar').after('<span class="error-holder">' + $(_password).data('message') + '</span>');
      return false;
    }

    // honeypot check for spam
    if (validateEmail(_email2.val())) {
      return false;
    }

    $(this).find('.has-error').removeClass('has-error');
    $(this).find('.error-holder').remove();

    // Check if request is already exist
    if (xhrLanding !== null && xhrLanding.readyState !== 4) return false;

    var lead = {
      email: _email.val(),
      first_name: _first_name.val(),
      last_name: _last_name.val(),
      password: _password.val(),
      provider: 'email',
      optin: _optin.is(':checked') ? true : false,
    };

    // Fb pixel value
    var emailao = lead.email;
    if (lead.email) {
      // Preloader
      var _page_id = $('.landing-v2-hero-form').data('page-id');
      // Set ajax data
      var data = {
        action: 'yova_create_leads',
        lead: lead,
        security: yova_ajax_params.security,
        page_id: _page_id,
        mediumSource: _medium_source,
      };

      makeLandingRequest(data);
    }
  });

  function makeLandingRequest(data) {
    var _email = $('.landing-form').find('[name="email"]');

    $('.landing-v2-hero-form').find('.preloader').addClass('active');

    $('.general-error-holder').attr('id', '');

    //console.log('fbq', fbq);
    // Make ajax request
    xhrLanding = $.post(yova_ajax_params.ajaxurl, data, function (response) {
      try {
        // console.log("response", response);

        $('.landing-v2-hero-form').find('.preloader').removeClass('active');

        response = JSON.parse(response);
        if (response.code === 200) {
          // track fb event

          if (typeof fbq !== 'undefined') {
            fbq('track', 'CompleteRegistration');
          }

          $('.landing-form').find('input').val('');

          // Add tracking id for GA TAG Manager
          if (isGoogleAuth) {
            $('.landing-form').find('#google-sign-up').prepend('<div id="google_lp_success"></div>');
          } else {
            $('.landing-form').find('button').prepend('<div id="lp_success"></div>');
          }
          // check if response has a redirection url change the url with it
          if (response.redirect_url) {
            window.location = response.redirect_url;
          } else {
            $('.general-error-holder').append(response.message);
          }
        } else {
          // check the same email error and show inline error
          // console.log("response", response);
          if (response.code == 400 || response.code == 422) {
            console.log('Error message: Code 400 ', _email);

            // if (isGoogleAuth) {
            // $(".general-error-holder").append(response.message);
            // $(".general-error-holder").attr("id", "error_same_email");
            // } else {
            $('.landing-form').find('.error-holder').remove();

            _email.addClass('has-error');

            $(_email).after('<span class="error-holder" id="error_same_email">' + response.message + '</span>');
            // }
          } else {
            $('.general-error-holder').append(response.message);
          }
        }
      } catch (error) {
        $('.general-error-holder').append(yova_ajax_params.error_message);
      }
    }).fail(function () {
      $('.general-error-holder').append(yova_ajax_params.error_message);
    });
  }
});

jQuery(document).ready(function ($) {
  var strength = {
    0: 'Very Weak',
    1: 'Weak',
    2: 'Medium',
    3: 'Good',
    4: 'Strong',
    5: 'Super Strong',
  };

  var rules = {
    min6: false,
    special: false,
    minCap: false,
    all: false,
    super: false,
  };

  var keys = Object.keys(rules);

  $('input[name=password]').on('input', function (e) {
    // console.log("input change", $(this).val());
    var password = $(this).val();
    var strengthIndicator = 0;
    // If the password length is less than or equal to 6
    if (password.length >= 6) {
      rules.min6 = true;
      $('.password-tip ul li:first-child').addClass('check');
      $('.password-field').find('.has-error').removeClass('has-error');
      $('.password-field').find('.error-holder').remove();
    } else {
      $('.password-tip ul li:first-child').removeClass('check');
      rules.min6 = false;
      $('.password-field').find('.has-error').addClass('has-error');
    }

    // Capitalize Letter
    if (password.match(/[A-Z]/)) {
      rules.minCap = true;
      $('.password-tip ul li:nth-child(3)').addClass('check');
    } else {
      $('.password-tip ul li:nth-child(3)').removeClass('check');
      rules.minCap = false;
    }

    // Number & Special
    if (password.match(/\d+/) && password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)) {
      rules.special = true;
      $('.password-tip ul li:nth-child(2)').addClass('check');
    } else {
      rules.special = false;
      $('.password-tip ul li:nth-child(2)').removeClass('check');
    }

    if (
      password.length > 6 &&
      ((password.match(/[a-z]/) && password.match(/\d+/)) ||
        (password.match(/\d+/) && password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)) ||
        (password.match(/[a-z]/) && password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)))
    ) {
      rules.all = true;
    } else {
      rules.all = false;
    }

    if (
      password.length > 12 &&
      password.match(/[a-z]/) &&
      password.match(/[A-Z]/) &&
      password.match(/\d+/) &&
      password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)
    ) {
      rules.super = true;
    } else {
      rules.super = false;
    }

    for (var rule in rules) {
      if (rules[rule] === true) {
        strengthIndicator++;
      }
    }

    // console.log("password", strengthIndicator, strength[strengthIndicator]);
    // console.log("rules", rules);

    var percent = (100 / keys.length) * strengthIndicator;
    percent = percent < 100 / keys.length ? 100 / keys.length : percent;
    percent = percent >= 100 ? 100 : percent;

    if (percent > 10 && percent <= 20) {
      $('.password-field').find('.password-info').text('Password is too weak.').removeClass('good medium super-strong strong');
    } else {
      $('.password-strength').removeClass('good medium strong super-strong');
    }

    if (percent > 20 && percent < 40) {
      $('.password-field').find('.error-holder').text('Password is good.');
      $('.password-strength').addClass('good');
      $('.password-field').find('.password-info').text('Password is good.').addClass('good').removeClass('medium strong super-strong');
    } else {
      $('.password-strength').removeClass('medium strong super-strong');
    }

    if (percent >= 40 && percent < 60) {
      $('.password-strength').addClass('medium');
      $('.password-field').find('.password-info').text('Password is medium.').addClass('medium').removeClass('strong super-strong');
    } else {
      $('.password-strength').removeClass('medium');
    }

    if (percent >= 60 && percent < 80) {
      $('.password-strength').addClass('strong');
      $('.password-field').find('.password-info').text('Password is strong.').addClass('strong');
    } else {
      $('.password-strength').removeClass('strong');
    }

    if (percent >= 80) {
      $('.password-strength').addClass('super-strong');
      $('.password-field').find('.password-info').text('Password is super-strong.').addClass('super-strong');
    } else {
      $('.password-strength').removeClass('super-strong');
    }

    if (!strengthIndicator) {
      $('.password-info').text('');
    }

    $('.password-strength').width(percent + '%');
  });
});

function trackingRegisterData(data) {
  var xhrLanding = null;

  if (xhrLanding !== null && xhrLanding.readyState !== 4) return false;
  // Make ajax request
  xhrLanding = $.post(yova_ajax_params.ajaxurl, data, function (response) {});
}
