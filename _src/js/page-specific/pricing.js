"use strict";

jQuery(document).ready(function ($) {
    // CAROUSEL
    var _resizeCheck = false;

    function makePricingBoxesCarousel() {
        var _target = $(".mobile-pricing-carousel");

        if ($(window).width() <= 812) {
            if (_target.hasClass("owl-loaded")) {
                return;
            }

            $(_target).owlCarousel({
                nav: false,
                dots: false,
                items: 1,
                center: true,
                autoWidth: true,
                margin: 10,
                loop: true,
            });

            $(".pricing-carousel-now .carousel-next-btn").click(function () {
                _target.trigger("next.owl.carousel");
            });
            // Go to the previous item
            $(".pricing-carousel-now .carousel-prev-btn").click(function () {
                _target.trigger("prev.owl.carousel");
            });

            _target.trigger("to.owl.carousel", [1, 300, true]);

            setTimeout(function () {
                var _w = $(_target).find(".owl-stage").width();

                $(_target).trigger("refresh.owl.carousel");

                $(_target)
                    .find(".owl-stage")
                    .width(_w + 10);
            }, 300);
        } else {
            _target.owlCarousel("destroy");
            _target.trigger("destroy.owl.carousel");
        }
    }

    $(window).resize(function () {
        makePricingBoxesCarousel();
    });

    // RANGE
    var slider = document.getElementById("myRange");

    var itemValue = 0;
    var stepValue = 0;
    var feeRange = [1.2, 1, 0.8, 0.6];
    var feeRange = $(".pricing-range").data("ratio-range").split(",");
    var ratioIndex = 0;

    updatePrice(slider.value * 200);

    // Update the current slider value (each time you drag the slider handle)
    slider.oninput = function () {
        if (this.value <= 10) {
            this.value = 10;
        }

        if (this.value < 250) {
            ratioIndex = 0;
            itemValue = 50000 / 250;
            stepValue = this.value * itemValue;
        }

        if (this.value >= 250 && this.value < 500) {
            ratioIndex = 1;
            itemValue = 100000 / 250;
            stepValue = 50000 + (this.value - 250) * itemValue;
        }
        if (this.value >= 500 && this.value < 750) {
            ratioIndex = 2;
            itemValue = 350000 / 250;
            stepValue = 150000 + (this.value - 500) * itemValue;
        }

        if (this.value >= 750 && this.value < 850) {
            ratioIndex = 3;
            itemValue = 500000 / 100;
            stepValue = 500000 + (this.value - 750) * itemValue;
        }

        if (this.value >= 850 && this.value <= 950) {
            ratioIndex = 3;
            itemValue = 1000000 / 100;
            stepValue = 1000000 + (this.value - 850) * itemValue;
        }

        if (this.value >= 950 && this.value <= 1000) {
            ratioIndex = 3;
            itemValue = 3000000 / 50;
            stepValue = 2000000 + (this.value - 950) * itemValue;
        }

        updatePrice(stepValue);
    };

    function updatePrice(value) {
        var _lang = document.documentElement.lang;
        var _currency = $(".pricing-range").data("ratio-currency");

        var updated = value.toLocaleString(_lang, {
            currency: _currency,
            minimumFractionDigits: 0,
            maximumFractionDigits: 0,
        });
        var cost = ((value * (feeRange[ratioIndex] / 100)) / 12).toLocaleString(_lang, {
            currency: _currency,
            minimumFractionDigits: 0,
            maximumFractionDigits: 0,
        });

        var note_range = $(".range-note").data("note-range").split(",");

        var active_note_index = 0;

        $(".investment-amount").html(updated);
        $(".monthly-cost").html(cost);
        $(".fee-range").html(feeRange[ratioIndex] + "%");
        $(".range-note > p").hide();

        if (cost <= parseInt(note_range[0], 10)) {
            active_note_index = 0;
        } else if (cost <= parseInt(note_range[1], 10)) {
            active_note_index = 1;
        } else if (cost <= parseInt(note_range[2], 10)) {
            active_note_index = 2;
        } else if (cost <= parseInt(note_range[3], 10)) {
            active_note_index = 3;
        } else if (cost <= parseInt(note_range[4], 10)) {
            active_note_index = 4;
        } else if (cost <= parseInt(note_range[5], 10)) {
            active_note_index = 5;
        } else {
            active_note_index = -1;
        }

        $(".range-note > p").eq(active_note_index).show();

        if (active_note_index == -1) {
            $(".range-note > p").hide();
        }
    }
});
