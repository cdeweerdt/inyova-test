jQuery(document).ready(function ($) {
  var xhrLanding = null;

  function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  $(".landing-form").on("submit", function (e) {
    e.preventDefault();

    $(".form-form-holder .general-error-holder").empty();

    $(this).find(".has-error").removeClass("has-error");

    $(this).find(".error-holder").remove();

    var _first_name = $(this).find('[name="first_name"]');
    var _email = $(this).find('[name="email"]');
    var _email2 = $(this).find('[name="email2"]');
    var _optin = $(this).find('[name="optin"]');

    var _medium_source = $(".landing-v2-hero-form").data("medium-source");

    if (_first_name.val().length < 1) {
      _first_name.addClass("has-error");
      $(_first_name).after(
        '<span class="error-holder">' +
          $(_first_name).data("message") +
          "</span>"
      );
    }

    _first_name.on("keyup change", function () {
      if (_first_name.val().length >= 1) {
        _first_name.removeClass("has-error");
        _first_name.next(".error-holder").remove();
      }
    });

    if (!validateEmail(_email.val())) {
      _email.addClass("has-error");
      $(_email).after(
        '<span class="error-holder">' + $(_email).data("message") + "</span>"
      );
    }

    _email.on("keyup change", function () {
      if (validateEmail(_email.val())) {
        _email.removeClass("has-error");
        _email.next(".error-holder").remove();
      }
    });

    // honeypot check for spam
    if (validateEmail(_email2.val())) {
      return false;
    }

    // clear button focus
    $('.landing-v2-btns-holder .btn').blur()
    //check if there are error 
    if ($(this).find(".has-error").length > 0) {
      return false;
    }

    $(this).find(".has-error").removeClass("has-error");
    $(this).find(".error-holder").remove();

    // Check if request is already exist
    if (xhrLanding !== null && xhrLanding.readyState !== 4) return false;

    var lead = {
      email: _email.val(),
      first_name: _first_name.val(),
      provider: "email",
      optin: _optin.is(":checked") ? true : false,
    };

    // Fb pixel value
    var emailao = lead.email;
    if (lead.email) {
      // Preloader
      var _page_id = $(".landing-v2-hero-form").data("page-id");
      // Set ajax data
      var data = {
        action: "yova_create_german_leads",
        lead: lead,
        security: yova_ajax_params.security,
        page_id: _page_id,
        mediumSource: _medium_source,
      };

      makeLandingRequest(data);
    }
  });

  function makeLandingRequest(data) {
    var _email = $(".landing-form").find('[name="email"]');

    $(".landing-v2-hero-form").find(".preloader").addClass("active");

    $(".general-error-holder").attr("id", "");

    // Make ajax request
    xhrLanding = $.post(yova_ajax_params.ajaxurl, data, function (response) {
      try {
        window.scrollTo(0, 0);
        $(".landing-v2-hero-form").find(".preloader").removeClass("active");

        response = JSON.parse(response);
        if (response.code === 200) {
          $(".landing-form").find("input").val("");

          // success class
          $(".form-form-holder").addClass("hide");
          $(".success-holder").addClass("show");
          $(".site").addClass("show-bg");
        } else {
          // check the same email error and show inline error
          if (response.code == 204) {

            $(".landing-form").find(".error-holder").remove();

            _email.addClass("has-error");

            $(_email).after(
              '<span class="error-holder" id="error_same_email">' +
                response.message +
                "</span>"
            );
          } else {
            $(".general-error-holder").append(response.message);
          }
        }
      } catch (error) {
        $(".general-error-holder").append(yova_ajax_params.error_message);
      }
    }).fail(function () {
      $(".general-error-holder").append(yova_ajax_params.error_message);
    });
  }
});
