jQuery(document).ready(function($) {

    var openedWindow; 
    setShareLinks();

    function socialWindow(url) {
        var left = (screen.width - 570) / 2;
        var top = (screen.height - 570) / 2;
        var params = "menubar=no,toolbar=no,status=no,width=570,height=570,top=" + top + ",left=" + left;
        // Setting 'params' to an empty string will launch
        // content in a new tab or window rather than a pop-up.
        // params = "";
        openedWindow = window.open(url, "NewWindow", params);
    }

    function setShareLinks() {
        //var pageUrl = encodeURIComponent(document.URL);
        var pageUrl = encodeURIComponent($('.waitlist-referral-link').val());
        var tweet = encodeURIComponent($("meta[property='og:description']").attr("content"));

        $(".social-share.facebook").on("click", function() {
            var url = "https://www.facebook.com/sharer.php?u=" + pageUrl;
            socialWindow(url);
        });

        $(".social-share.twitter").on("click", function() {
            var url = "https://twitter.com/intent/tweet?url=" + pageUrl;
            socialWindow(url);
        });

        $(".social-share.linkedin").on("click", function() {
            var url = "https://www.linkedin.com/shareArticle?mini=true&url=" + pageUrl;
            socialWindow(url);
        })

        $(".social-share.email").on("click", function() {
            var subject;

            switch ($('html').attr('lang')) {
                case 'de-DE':
                    subject = "Jetzt Early Access auf Inyova's nachhaltige 3a sichern.";
                    break;
                case 'fr-FR':
                    subject = "Accède en avant-première au pilier 3a durable d'Inyova.";
                    break;
                case 'en-US':
                default:
                    subject = "Get Early Access to Inyova's sustainable 3a now.";
                    break;
            }

            var url = "mailto:?subject=" + subject + "&body=" + pageUrl;
            socialWindow(url);
            setTimeout(function() {
                openedWindow.close();
            }, 500);
        })
        $(".social-share.whatsapp").on("click", function() {
            var url = "https://api.whatsapp.com/send?text=" + pageUrl;
            socialWindow(url);
        })
    }
});
