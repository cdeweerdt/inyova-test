"use strict";

jQuery(document).ready(function($) {
  // clear email2 honeypot input on opening
  $(this)
    .find('[name="email2"]')
    .val("");

  $(".js-google-back").on("click", function(e) {
    e.preventDefault();
    $(".google-form-holder").hide();
    $(".form-form-holder").show();
    $(".landing-btns-holder").css("display", "flex");

    $("landing-form input").val("");
    $(".landing-form").show();

    $(".landing-form")
      .find(".has-error")
      .removeClass("has-error");

    $(".landing-hero-form")
      .find(".general-error-holder")
      .val("");
  });

  var xhrLanding = null;

  function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  $(".landing-form").on("submit", function(e) {
    e.preventDefault();
    // se false
    window.googleSignIn = false;

    $(this)
      .find(".has-error")
      .removeClass("has-error");
    $(this)
      .find(".error-holder")
      .remove();
    $(".landing-form")
      .find(".lds-dual-ring")
      .remove();
    $(this)
      .find("button")
      .removeAttr("disabled");
    $(this)
      .find("button")
      .removeClass("disabled");

    var _first_name = $(this).find('[name="first_name"]');
    var _email = $(this).find('[name="email"]');
    var _email2 = $(this).find('[name="email2"]');
    var _password = $(this).find('[name="password"]');

    var _medium_source = $(".landing-hero-form").data("medium-source");

    if (_first_name.val().length < 1) {
      _first_name.addClass("has-error");
      $(_first_name).after(
        '<span class="error-holder">' +
          $(_first_name).data("message") +
          "</span>"
      );
      return false;
    }

    if (!validateEmail(_email.val())) {
      _email.addClass("has-error");
      $(_email).after(
        '<span class="error-holder">' + $(_email).data("message") + "</span>"
      );
      return false;
    }

    if (_password.val().length < 1) {
      _password.addClass("has-error");
      $(_password).after(
        '<span class="error-holder">' + $(_password).data("message") + "</span>"
      );
      return false;
    }

    // honeypot check for spam
    if (validateEmail(_email2.val())) {
      return false;
    }

    $(this)
      .find(".has-error")
      .removeClass("has-error");
    $(this)
      .find(".error-holder")
      .remove();

    $(this)
      .find("button")
      .attr("disabled", "disabled");
    $(this)
      .find("button")
      .addClass("disabled");
    $(this)
      .find("button")
      .prepend('<div class="lds-dual-ring" id="lp_success"></div>');

    // Check if request is already exist
    if (xhrLanding !== null && xhrLanding.readyState !== 4) return false;

    var lead = {
      email: _email.val(),
      first_name: _first_name.val(),
      password: _password.val()
    };

    // Fb pixel value
    var emailao = lead.email;
    if (lead.email) {
      // Preloader

      // Set ajax data
      var data = {
        action: "yova_create_leads",
        lead: lead,
        security: yova_ajax_params.security,
        page_id: yova_ajax_params.page_id,
        mediumSource: _medium_source
      };

      //   consl
      makeLandingRequest(data);
    }
  });

  function makeLandingRequest(data) {
    var _email = $(".landing-form").find('[name="email"]');

    //console.log('fbq', fbq);
    // Make ajax request
    xhrLanding = $.post(yova_ajax_params.ajaxurl, data, function(response) {
      try {
        // console.log("response", response);

        response = JSON.parse(response);
        if (response.code === 200) {
          // track fb event
          fbq("track", "CompleteRegistration");

          $(".landing-form")
            .find("input")
            .val("");
          // Add tracking id for GA TAG Manager
          $(this)
            .find("button")
            .prepend('<div id="lp_success"></div>');

          // check if response has a redirection url change the url with it
          if (response.redirect_url) {
            window.location = response.redirect_url;
          } else {
            $(".general-error-holder").append(response.message);
          }
        } else {
          // check the same email error and show inline error

          if (response.code == 400) {
            // console.log("Error message: Code 400 ", _email);

            $(".landing-form")
              .find(".error-holder")
              .remove();

            _email.addClass("has-error");

            $(_email).after(
              '<span class="error-holder" id="error_same_email">' +
                response.message +
                "</span>"
            );
          } else {
            $(".general-error-holder").append(response.message);
          }
        }

        clearform();
      } catch (error) {
        clearform();
        $(".general-error-holder").append(yova_ajax_params.error_message);
      }
    }).fail(function() {
      clearform();
      $(".general-error-holder").append(yova_ajax_params.error_message);
    });
  }

  function clearform() {
    $(".landing-form")
      .find(".lds-dual-ring")
      .remove();
    $(".landing-form")
      .find("button")
      .removeAttr("disabled");
    $(".landing-form")
      .find("button")
      .removeClass("disabled");
    // clear google sign-up class
    $("#google-sign-up").removeClass("disabled");
  }
});
