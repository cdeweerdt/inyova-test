jQuery(document).ready(function($) {
    // console.log("approach-scroll");

    $(window).resize(function() {
        $(window).scroll();
    });

    $(".scrollable-image").eq(0).addClass("is-active");
    $(".dot").eq(0).addClass("is-active");

    var activeState = 0;
    var lastScrollTop = 0;
    //Scrollable section


    $(".dots-nav .dot").on("click", function() {
        var index = $(this).index();
        var section_height = $(".scrollable-section").height();
        var _holder_position = $(".scrollable-sections.desktop").position();

        $(".scrollable-image").removeClass("is-active");

        $(".scrollable-image").eq(index).addClass("is-active");

        $(".dots-nav .dot").removeClass("is-active");
        $(".dots-nav .dot").eq(index).addClass("is-active");

        $("html, body")
            .stop()

    });

    $(".scrollable-section-carousel").owlCarousel({
        nav: false,
        dots: true,
        margin: 10,
        items: 1,
        onChanged: function(event) {
            // console.log(event.item.count, event.item.index);
            $(".carousel-item-content").hide();
            $(".carousel-item-content").eq(event.item.index).show();
        },
    });

    // mobile carousel

    // caoursel
    $(".approach-info-carousel").owlCarousel({
        nav: false,
        dots: true,
        items: 1,
    });

    $(".carousel-nav .carousel-next-btn").click(function(e) {
        $(".approach-info-carousel").trigger("next.owl.carousel");
    });
    // Go to the previous item
    $(".carousel-nav .carousel-prev-btn").click(function(e) {
        $(".approach-info-carousel").trigger("prev.owl.carousel");
    });
});