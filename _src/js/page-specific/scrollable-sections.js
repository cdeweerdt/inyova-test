jQuery(document).ready(function ($) {
  // console.log("approach-scroll");

  $(window).resize(function () {
    $(window).scroll();
  });

  $(".scrollable-image").eq(0).addClass("is-active");

  var activeState = 0;
  var lastScrollTop = 0;
  //Scrollable section
  $(window).scroll(function (e) {
    var scroll = $(window).scrollTop();

    // console.log(scroll);

    var _target = $(".scrollable-sections").position();
    var maxThershold = _target.top + $(".scrollable-sections").height();
    // console.log(_target, maxThershold);
    var scrollableAreaTop = _target.top - $(window).height();

    var scrollableAreaTop =
      _target.top +
      $(".scrollable-sections").height() -
      $(".scrollable-section").height();

    if (scroll <= scrollableAreaTop) {
      $(".scrollable-images-holder").addClass("is-top");
      $(".scrollable-images-holder").removeClass("is-fixed");
    }

    if (scroll >= _target.top && scroll <= maxThershold) {
      $(".scrollable-images-holder").addClass("is-fixed");
      $(".scrollable-images-holder").removeClass("is-top");
      $(".scrollable-images-holder").removeClass("is-bottom");
      // $(".scrollable-image").eq(0).addClass("is-active");
    } else {
      $(".scrollable-images-holder").removeClass("is-fixed");
    }

    if (scroll >= scrollableAreaTop) {
      $(".scrollable-images-holder").removeClass("is-fixed");
      $(".scrollable-images-holder").addClass("is-bottom");
    }

    $(".scrollable-section").each(function (index) {
      var _holder_target = $(".scrollable-sections").position();
      var _section_position = $(this).height();
      // console.log(index, _section_position);

      if (
        scroll >=
        _holder_target.top + _section_position * index - _section_position / 2
      ) {
        activeState = index;
        $(".scrollable-image").removeClass("is-active");

        $(".scrollable-image").eq(index).addClass("is-active");

        $(".dots-nav .dot").removeClass("is-active");
        $(".dots-nav .dot").eq(index).addClass("is-active");
      }
    });

    // update last scroll position
    lastScrollTop = scroll;
  });

  $(".dots-nav .dot").on("click", function () {
    var index = $(this).index();
    var section_height = $(".scrollable-section").height();
    var _holder_position = $(".scrollable-sections.desktop").position();

    $("html, body")
      .stop()
      .animate(
        { scrollTop: _holder_position.top + section_height * index },
        500,
        "swing"
      );
  });

  $(".scrollable-section-carousel").owlCarousel({
    nav: false,
    dots: true,
    margin: 10,
    items: 1,
    onChanged: function (event) {
      // console.log(event.item.count, event.item.index);
      $(".carousel-item-content").hide();
      $(".carousel-item-content").eq(event.item.index).show();
    },
  });

  // mobile carousel

  // caoursel
  $(".approach-info-carousel").owlCarousel({
    nav: false,
    dots: true,
    items: 1,
  });

  $(".carousel-nav .carousel-next-btn").click(function (e) {
    $(".approach-info-carousel").trigger("next.owl.carousel");
  });
  // Go to the previous item
  $(".carousel-nav .carousel-prev-btn").click(function (e) {
    $(".approach-info-carousel").trigger("prev.owl.carousel");
  });
});
