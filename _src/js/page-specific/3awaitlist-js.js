jQuery(document).ready(function($) {
    
    // Force remove value of input
    $('#acf-ch-3a_waitlist_email_acf').removeAttr('value');

    // Disable the ACF js navigate away pop up
    if (typeof acf !== 'undefined') {
        acf.unload.active = false;
    }
    

    // Add class for label animation
    $('#acf-ch-3a_waitlist_email_acf').on('input', function() {
        if ($(this).val() != '') {
            $(this).closest('.acf-field').addClass('focused');
        } else {
            $(this).closest('.acf-field').removeClass('focused');
        }
    });

    // Ajax request on submission for login
    $('#acf-form-yova-3a-waitlist-login').on('submit', function(e) {
        e.preventDefault();
        const $this = $(this);

        $.ajax({
            data: {
                'email': $('#acf-ch-3a_waitlist_email_acf').val(),
                'nonce': rest.nonce,
                'post_id': rest.post_id
            },
            dataType: 'JSON',
            method: 'POST',
            url: rest.url + 'yova-3a-waitlist/login/'
        }).done(function(data, textStatus, jqXHR) {
            if (data.error === true) {
                if ($this.find('.acf-input-feedback').length) {
                    $this.find('.acf-input-feedback').remove();
                    $this.find('.acf-field').removeClass('error');
                }

                $this.find('.acf-field').addClass('error');
                $this.find('#acf-ch-3a_waitlist_email_acf').addClass('error');
                $this.find('.acf-input-wrap').append('<div class="acf-input-feedback error"></div>');
                $this.find('.acf-input-feedback').html(data.message);
            } else {
                window.location.href = data.redirect;
            }
        });
    });
    
    // Ajax request on submission for sign up
    $('#acf-form-yova-3a-waitlist-signup').on('submit', function(e) {
        e.preventDefault();
        const $this = $(this);

        const dataToSend = {
            'email': $('#acf-ch-3a_waitlist_email_acf').val(),
            'nonce': rest.nonce,
            'post_id': rest.post_id
        };

        if ( $('#acf-form-yova-3a-waitlist-signup input[name="referral"]').val() != '' ) {
            dataToSend.referral = $('#acf-form-yova-3a-waitlist-signup input[name="referral"]').val();
        }

        $.ajax({
            data: dataToSend,
            dataType: 'JSON',
            method: 'POST',
            url: rest.url + 'yova-3a-waitlist/signup/'
        }).done(function(data, textStatus, jqXHR) {
            if (data.error === true) {
                if ($this.find('.acf-input-feedback').length) {
                    $this.find('.acf-input-feedback').remove();
                    $this.find('.acf-field').removeClass('error');
                }
                
                $this.find('.acf-field').addClass('error');
                $this.find('#acf-ch-3a_waitlist_email_acf').addClass('error');
                $this.find('.acf-input-wrap').append('<div class="acf-input-feedback error"></div>');
                $this.find('.acf-input-feedback').html(data.message);
            } else {
                window.location.href = data.redirect;
            }
        });
    });

    // Scroll to hash element
    $('a[href="#waitlist-referral"]').on( 'click', function( e ) {
        e.preventDefault();
        $( 'html, body' ).animate({
            scrollTop: $( '#waitlist-referral' ).offset().top - 40
        }, 1000, 'swing');
    });

    // Copy referral link to clipboard
    $('.waitlist-referral-copy').on( 'click', function() {
        const element = $(this).prev('.waitlist-referral-link')[0];
        element.select();
        element.setSelectionRange(0, 90000000);
        document.execCommand('copy');
    });

    if ($('.swiper-container').length) {
        const swiper = new Swiper('.swiper-container', {
            direction: 'horizontal',
            slidesPerView: 1,
            mousewheel: true,
            spaceBetween: 50,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            breakpoints: {
                768: {
                    direction: 'vertical',
                    spaceBetween: 0
                }
              }
        });
    }
});