jQuery(document).ready(function($) {
  var xhrLanding = null;

  window.googleSignIn = true;
  var second_button_clicked = false;
  var googleUser = {};
  var _auth2;

  var startGoogleAuth = function() {
    // console.log("startGoogleAuth");
    gapi.load("auth2", function() {
      _auth2 = gapi.auth2.init({
        client_id: yova_ajax_landing_params.google_client_id,
        cookiepolicy: "single_host_origin"
        // Request scopes in addition to 'profile' and 'email'
        //scope: 'additional_scope'
      });
      enableGoogleButton();
    });
  };

  function enableGoogleButton() {
    // in form google sign-up button
    _auth2.attachClickHandler(
      document.getElementById("google-sign-up-2"),
      {},
      function(googleUser) {
        googleSignIn(googleUser);
      },
      function(error) {
        console.log(
          "Error message auth-2",
          JSON.stringify(error, undefined, 2)
        );
      }
    );

    _auth2.attachClickHandler(
      document.getElementById("google-sign-up"),
      {},
      function(googleUser) {
        googleSignIn(googleUser);
      },
      function(error) {
        console.log(JSON.stringify(error, undefined, 2));
        $("#google-sign-up").removeClass("disabled");
        $("#google-sign-up")
          .find(".lds-dual-ring")
          .remove();
      }
    );
  }

  function googleSignIn(googleUser) {
    var _profile = googleUser.getBasicProfile();
    var _uid = _profile.getId();
    var _name = _profile.getName();
    var _email = _profile.getEmail();

    var _medium_source = $(".landing-hero-form").data("medium-source");

    window.googleSignIn = true;

    // clear form
    $(".landing-form")
      .find(".error-holder")
      .remove();
    $(".landing-form")
      .find(".has-error")
      .removeClass("has-error");

    // // update fields

    // Check if request is already exist
    // if (xhrLanding !== null && xhrLanding.readyState !== 4) return false;

    var lead = {
      email: _email,
      first_name: _name,
      uid: _uid,
      password: "",
      provider: "google"
    };

    var emailao = lead.email;
    if (lead.email) {
      // Set ajax data
      var data = {
        action: "yova_create_leads",
        lead: lead,
        security: yova_ajax_params.security,
        page_id: yova_ajax_params.page_id,
        mediumSource: _medium_source
      };

      // landing.js
      makeLandingRequest(data);
    }
  }

  $("#google-sign-up-2").on("click", function() {
    second_button_clicked = true;
    $(".landing-form")
      .find("input")
      .val("");
    $(".landing-form")
      .find(".error-holder")
      .remove();
    $(".landing-form")
      .find(".has-error")
      .removeClass("has-error");

    $(".google-auth-error-holder").val("");
  });

  $("#google-sign-up").on("click", function() {
    second_button_clicked = false;
    $(".landing-form")
      .find("input")
      .val("");
    $(".landing-form")
      .find(".error-holder")
      .remove();

    $(".js-google-signup").removeClass("has-error");
    $(".google-auth-error-holder").val("");

    if (!$(this).hasClass("disabled")) {
      $(this)
        .addClass("disabled")
        .prepend('<div class="lds-dual-ring" id="google_lp_success"></div>');
    }
  });

  startGoogleAuth();

  function makeLandingRequest(data) {

    $(".google-auth-error-holder").val("");
    // console.log("makeLandingRequest", data);
    // Make ajax request
    xhrLanding = $.post(yova_ajax_params.ajaxurl, data, function(response) {
      try {
        // console.log("response", response);

        response = JSON.parse(response);

        if (response.code === 200) {
          // track fb event
          fbq("track", "CompleteRegistration");

          // console.log("Error message: Code 200", response);
          $(".landing-form")
            .find("input")
            .val("");
          // Add tracking id for GA TAG Manager
          $(this)
            .find("button")
            .prepend('<div id="lp_success"></div>');

          // check if response has a redirection url change the url with it
          if (response.redirect_url) {
            window.location = response.redirect_url;
          } else {
            // clearform();
            // Yova.showPopup(response.message);
            $(".google-auth-error-holder").append(response.message);
          }
        } else {
          // check the same email error and show inline error

          if (response.code == 400) {
            // console.log("Error message: Code 400", response);

            if (second_button_clicked) {
              $(".google-form-holder").hide();
              $(".landing-form").show();
              $(".landing-btns-holder").show();

              $(".general-error-holder").append(response.message);
            } else {
              $(".landing-form")
                .find(".error-holder")
                .remove();

              $(".google-form-holder").show();
              $(".landing-form").hide();
              $(".landing-btns-holder").hide();

              $("#google-sign-up").addClass("disabled");
              $(".google-auth-error-holder").append(response.message);
            }

            clearform();
          } else {
            clearform();
            // Yova.showPopup(response.message);
            // console.log("Error message: Others", response);

            $(".google-form-holder .google-auth-error-holder").append(
              response.message
            );
          }
        }

        clearform();
      } catch (error) {
        // console.log("Error: ", error);
        clearform();
        $(".google-auth-error-holder").append(yova_ajax_params.error_message);
        // Yova.showPopup(yova_ajax_params.error_message);
      }
    }).fail(function() {
      // console.log("Xhr request fail" + yova_ajax_params.error_message);
      clearform();
      $(".google-auth-error-holder").append(yova_ajax_params.error_message);
      //   Yova.showPopup(yova_ajax_params.error_message);
    });
  }

  function clearform() {
    $(".landing-form")
      .find(".lds-dual-ring")
      .remove();
    $(".landing-form")
      .find("button")
      .removeAttr("disabled");
    $(".landing-form")
      .find("button")
      .removeClass("disabled");

    $(".js-google-signup")
      .find(".lds-dual-ring")
      .remove();
    // clear google sign-up class
    $("#google-sign-up").removeClass("disabled");
  }
});
