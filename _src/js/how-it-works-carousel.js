"use strict";

jQuery(document).ready(function($) {
  $(window).resize(function() {
    if ($(".how-it-works-phones-holder.landing-growth").length > 0) {
      $(".how-it-works-phones-holder").trigger("destroy.owl.carousel");

      if ($(window).width() <= 768) {
        $(".how-it-works-phones-holder.landing-growth").owlCarousel({
          items: 1,
          dots: true,
          onChange: function(event) {
            var currentItem = event.item.index;
            $(".how-it-works-mobile-description p").hide();
            $(".how-it-works-mobile-description")
              .find("p")
              .eq(currentItem)
              .fadeIn();
          }
        });
      }
    }
  });

  $(window).trigger("resize");
});
