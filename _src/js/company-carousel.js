"use strict";

jQuery(document).ready(function($) {
  const _target = $(".company-carousel");
  if ($(".company-carousel").length) {
    $(_target).owlCarousel({
      nav: false,
      dots: false,
      drag: false,
      responsive: {
        0: {
          items: 2,
          slideBy: 2,
          margin: 56
        },
        768: {
          margin: 56,
          slideBy: 5,
          items: 5
        },
        1024: {
          margin: 56,
          slideBy: 5,
          items: 5
        }
      }
    });

    $(".carousel-next-btn").click(function() {
      _target.trigger("next.owl.carousel");
    });
    // Go to the previous item
    $(".carousel-prev-btn").click(function() {
      _target.trigger("prev.owl.carousel");
    });
  }
});
