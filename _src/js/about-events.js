"use strict";

jQuery(document).ready(function($) {
  var $event_carousel = $(".about-event-carousel");

  $(window).resize(function() {
    // console.log("resize");
    if ($event_carousel.length > 0) {
      $event_carousel.trigger("destroy.owl.carousel");

      if ($(window).width() <= 768) {
        var $padding = 100;
        if ($(window).width() < 480) {
          $padding = 40;
        }
        if ($(window).width() < 360) {
          $padding = 20;
        }

        $event_carousel.owlCarousel({
          stagePadding: $padding,
          items: 1,
          margin: 10,
          dots: false,
          nav: false,
          drag: false
        });
      }
    }
  });

  $(".about-events-nav")
    .find(".carousel-next-btn")
    .click(function(e) {
      e.preventDefault();
      console.log("next");
      $event_carousel.trigger("next.owl.carousel");
    });
  // Go to the previous item
  $(".about-events-nav")
    .find(".carousel-prev-btn")
    .click(function(e) {
      e.preventDefault();
      console.log("prev");
      $event_carousel.trigger("prev.owl.carousel");
    });

  //   $(window).trigger("resize");
});
