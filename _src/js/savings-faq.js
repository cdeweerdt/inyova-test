"use strict";

jQuery(document).ready(function($) {
  $(".js-savings-faq").on("click", function() {
    $(".savings-faq-sidebar")
      .find(".active")
      .removeClass("active");
    $(this).addClass("active");

    var _id = $(this).data("id");
    // console.log(_id);
    $(".faq-item.active").removeClass("active");
    $('.faq-item[data-id="' + _id + '"]').addClass("active");
  });
});
