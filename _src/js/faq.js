"use strict";

jQuery(document).ready(function($) {
  $(".js-change-faq-category").on("click", function() {
    $(".faq-sidebar .js-change-faq-category.active").removeClass("active");
    $(this).addClass("active");

    var _id = $(this).data("id");
    // console.log(_id);
    $(".faq-category-content.active").removeClass("active");
    $('.faq-category-content[data-id="' + _id + '"]').addClass("active");
  });

  $(".faq-question-holder").on("click", function() {
    $(this).toggleClass("active");

    $(this)
      .find(".faq-answer")
      .slideToggle(300);
  });

  $(".js-mobile-faq-category").on("click", function(e) {
    e.preventDefault();
    var _id = $(this).data("id");
    //console.log(_id);
    $(".faq-category-content.active").removeClass("active");
    $('.faq-category-content[data-id="' + _id + '"]').addClass("active");

    var scrollTop = $('.faq-category-content[data-id="' + _id + '"]').offset()
      .top;
    $(window).scrollTop(scrollTop - $(".site-header").outerHeight() - 10);
  });
});
