"use strict";

jQuery(document).ready(function($) {
  const _target = $(".delighted-carousel-content");
  var _temp_click = null;

  if (_target.length) {
    $("body").on("click", ".testimonial-item-read-more", function() {
      $(".testimonial-item-user-review").removeClass("active");
      $(".testimonial-item-read-more").show();

      var index = $(this)
        .closest(".owl-item")
        .index();

      $(this).blur();

      if (_temp_click != index) {
        $(this)
          .parent()
          .find(".testimonial-item-user-review")
          .addClass("active");

        $(this).hide();
      }

      _temp_click = null;
    });
    //delighted-carousel-content
    $(_target).owlCarousel({
      nav: false,
      dots: false,
      onDragged: function(e) {
        // console.log("current: ", e); //same
        $(".delighted-testimonial-statement").removeClass("active");
        $(".delighted-read-more").show();
        _temp_click = null;
      },
      responsive: {
        0: {
          items: 1,
          margin: 10,
          dots: true,
          stagePadding: 20
        },
        812: {
          margin: 10,
          items: 2,
          dots: true
        },
        1280: {
          margin: 30,
          items: 3
        },
        1440: {
          margin: 30,
          items: 4
        }
      }
    });

    $(".delighted-carousel-nav .carousel-next-btn").click(function(e) {
      _target.trigger("next.owl.carousel");
    });
    // Go to the previous item
    $(".delighted-carousel-nav .carousel-prev-btn").click(function(e) {
      _target.trigger("prev.owl.carousel");
    });
  }
});
