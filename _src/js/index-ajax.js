var $ = jQuery.noConflict();
("use strict");

function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

var xhrSubscribe = null;

$(".newsletter-form").each(function() {
  $(this).on("submit", function(e) {
    // console.log('submit');
    e.preventDefault();

    $(".footer-company-mobile").removeClass("has-response");

    // console.log("form submit");
    var _id = $(this)
      .parent()
      .attr("id");

    $("#" + _id)
      .find(".form-message")
      .remove();

    // Check if request is already exist
    if (xhrSubscribe !== null && xhrSubscribe.readyState !== 4) return false;

    //honeypot check
    var honeypot = $("#" + _id)
      .find('[name="email2"]')
      .val();

    if (honeypot != "") {
      // early exit
      return;
    }

    var email = $("#" + _id)
      .find('[name="email"]')
      .val();

    // Fb pixel value
    var emailao = email;
    if (email) {
      // set action type
      var medium_type = "Newsletter";
      if (_id == "career") {
        medium_type = "Jobs";
      }

      if (_id.indexOf("french-landing") != -1) {
        medium_type = "French";
      }

      // Set ajax data
      var data = {
        action: "yova_subscribe",
        security: yova_ajax_params.security,
        email: email,
        page_id: yova_ajax_params.page_id,
        medium_type: medium_type
      };

      // console.log(data);

      // Make ajax request
      xhrSubscribe = $.post(yova_ajax_params.ajaxurl, data, function(response) {
        try {
          response = JSON.parse(response);

          $(".footer-company-mobile").addClass("has-response");
          if (response) {
            if (response.status === "success") {
              $(".newsletter-form")
                .find('[name="email"]')
                .val("");
            }
            // Yova.showPopup(response.message);
            $("#" + _id).append(
              '<div class="form-message ' +
                response.status +
                '">' +
                response.message +
                "</div>"
            );
          } else {
            //general error
            $("#" + _id).append(
              '<div class="form-message error">' +
                yova_ajax_params.error_message +
                "</div>"
            );
          }
        } catch (error) {
          //general error
          $("#" + _id).append(
            '<div class="form-message error">' +
              yova_ajax_params.error_message +
              "</div>"
          );
        }
      }).fail(function() {
        //general error
        $("#" + _id).append(
          '<div class="form-message error">' +
            yova_ajax_params.error_message +
            "</div>"
        );
      });
    }
  });
});
