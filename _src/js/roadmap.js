"use strict";

var xhrAddLike = null;

jQuery(document).ready(function($) {
  if ($(".roadmap-box").length) {
    $(".roadmap-box").each(function(index) {
      var height = $(this)
        .find(".roadmap-content")
        .outerHeight();

      $(this)
        .find(".roadmap-content")
        .css("height", height);
    });
  }

  $(".like-btn").on("click", function() {
    // Check if request is already exist
    var _btn = $(this);
    if (xhrAddLike !== null && xhrAddLike.readyState !== 4) return false;

    var data = {
      action: "yova_add_like",
      security: yova_ajax_params.security,
      user_action: $(_btn).hasClass("liked") ? "dislike" : "like",
      page_id: $(this).data("page")
    };

    xhrAddLike = $.post(yova_ajax_params.ajaxurl, data, function(response) {
      try {
        response = JSON.parse(response);
        if (response.status == "success") {
          // console.log(response);
          $(_btn)
            .find(".like-count")
            .html(response.like_count);
          // update class
          if (response.user_action == "like") {
            $(_btn).addClass("liked");
          } else {
            $(_btn).removeClass("liked");
          }
        }
      } catch (error) {}
    }).fail(function() {});
  });
});
