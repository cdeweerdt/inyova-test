"use strict";

jQuery(document).ready(function($) {
  // sticky testimonial sidebar
  if ($(".testimonial-sidebar").length > 0) {
    if ($(window).width() > 768) {
      $(".testimonial-sidebar").stickySidebar({
        topSpacing: parseInt(
          $(".single-testimonial-holder").css("margin-top"),
          10
        ),
        bottomSpacing: 60
      });
    } else {
      $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll >= 120) {
          $(".mobile-testimonial-nav").addClass("active");
        } else {
          $(".mobile-testimonial-nav").removeClass("active");
        }
      });
    }
  }

  // testimonial carousel
  function makeCarousel() {
    if ($(".testimonial-images").length > 0) {
      // console.log("makeCarousel");
      var _w = $(window).width();

      $(".testimonial-images").trigger("destroy.owl.carousel");

      var $testimonial_image_carousel = $(".testimonial-images").owlCarousel({
        margin: 60,
        rtl: true,
        nav: false,
        dots: false,
        loop: true,
        mouseDrag: false,
        touchDrag: false,
        pullDrag: false,
        freeDrag: false,
        responsive: {
          680: {
            items: 2
          },
          1920: {
            items: 3
          }
        }
      });

      var $testimonial_content_carousel = $(
        ".testimonial-contents"
      ).owlCarousel({
        items: 1,
        nav: false,
        dots: false,
        loop: true,
        mouseDrag: false,
        touchDrag: false,
        pullDrag: false,
        freeDrag: false
      });

      $(".testimonial-nav")
        .find(".carousel-next-btn")
        .click(function() {
          $testimonial_image_carousel.trigger("next.owl.carousel");
          $testimonial_content_carousel.trigger("next.owl.carousel");
        });
      // Go to the previous item
      $(".testimonial-nav")
        .find(".carousel-prev-btn")
        .click(function() {
          $testimonial_image_carousel.trigger("prev.owl.carousel");
          $testimonial_content_carousel.trigger("prev.owl.carousel");
        });

      $testimonial_content_carousel.trigger("refresh.owl.carousel");
    }
  }

  makeCarousel();
});
