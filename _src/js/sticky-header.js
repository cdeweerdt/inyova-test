jQuery(document).ready(function($) {
  // console.log("sticky header");
  $(window).scroll(function() {
    var sticky = $(".sticky"),
      scroll = $(window).scrollTop();

    var endofPage =
      $(document).height() -
      $(".site-footer").outerHeight() -
      $(".site-header").outerHeight() +
      60;

    if (scroll > endofPage) sticky.addClass("hide-sticky");
    else sticky.removeClass("hide-sticky");
    if (scroll >= 16 && scroll) sticky.addClass("sticky-shadow");
    else sticky.removeClass("sticky-shadow");
  });
});
